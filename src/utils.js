// eslint-disable-next-line
export const serialize = data =>
	Object.keys(data).reduce((acc, key) =>
		[...acc, `${key}=${data[key]}`], []).join('&');
