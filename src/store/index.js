import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
	todos: [],
	count: 0,
};

const getters = {
	getTodos: ({ todos }) => ({ status, name }) =>
		todos.filter((item) => {
			let result = true;
			if (name) {
				result = name === item.name;
			}

			if (status) {
				result = result && status === item.status;
			}

			return result;
		}),
};

const mutations = {
	addTodo({ todos }, todo) {
		state.todos = [...todos, todo];
	},
	deleteTodo({ todos }, id) {
		state.todos = todos.reduce((acc, item) =>
			(item.id === id ? acc : [...acc, item]), []);
	},
	setName({ todos }, { id, name }) {
		state.todos = todos.map(item =>
			(item.id === id ? { ...item, name }
				: item));
	},
	setStatus({ todos }, { id, status }) {
		state.todos = todos.map(item =>
			(item.id === id ? { ...item, status: status ? 'active' : 'nonactive' }
				: item));
	},
	countInc({ count }) {
		state.count = count + 1;
	},
};

export default new Vuex.Store({
	state,
	getters,
	mutations,
});
